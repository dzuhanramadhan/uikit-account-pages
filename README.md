# Account pages
> ### UIkit Framework account pages template
[![License](https://img.shields.io/github/license/dzuhanramadhan/uikit-account-pages)](https://github.com/dzuhanramadhan/uikit-account-pages/blob/main/LICENSE)
[![author](https://img.shields.io/badge/author-%40dzuhanramadhan-blue)](https://github.com/dzuhanramadhan/)
[![GitHub followers](https://img.shields.io/github/followers/dzuhanramadhan?label=follow%20Us&style=social)](https://github.com/dzuhanramadhan/)
[![Twitter Follow](https://img.shields.io/twitter/follow/dzuhanramadhan1?label=Follow&style=social)](https://twitter.com/dzuhanramadhan1)

We use the [UIkit](https://getuikit.com/) framework by prioritizing a minimalist style and authenticity of a simple framework design

# Demo
* ### [Login page](https://dzuhanramadhan.github.io/uikit-account-pages/login%20page/index.html)
Simple and minimalist login page for your personal anda commercial projects. You can get it by following the [#installation](#installation)

[![](https://dzuhanramadhan.github.io/uikit-account-pages/login%20page/image/preview-page.png)](https://dzuhanramadhan.github.io/uikit-account-pages/login%20page/index.html)

* ### [SignUp page](#) (Coming soon)

# Installation
* Open and copy this in your git bash (It's up to you where you want to be)
```
git clone https://github.com/dzuhanramadhan/uikit-account-pages.git
```
* Enter into your directory
```
cd uikit-account-pages
```
* Open your favorite code editor software and add folder. If use sublime text 3 run code
```
alias subl="location of your sublime text installation"
```
Then
```
subl .
```
or
* Open your favorite code editor software and drag your repo folder

# Features
* [UIkit framework](https://getuikit.com)
* Personal and commercial use
* Minify CSS & JS
* Clean & simple design
* Fully responsive
* Cross-browser compatibility
* Easy to use & edit
* Show/hide password

# License
[![License](https://img.shields.io/github/license/dzuhanramadhan/uikit-account-pages)](https://github.com/dzuhanramadhan/uikit-account-pages/blob/main/LICENSE)

# Donation
By donating, you have appreciated our work and our hard work to make us even more enthusiastic about making other useful 

[![](https://img.shields.io/badge/Donate-PayPal-blue)](https://www.paypal.me/dzuhanramadhan)
[![](https://img.shields.io/badge/Donate-Buy%20me%20a%20Coffee-yellow)](https://www.buymeacoffee.com/dzuhanramadhan)
